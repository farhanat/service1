package com.oracle.external.service.endpoint;

import com.oracle.external.dto.ResponseDTO;
import com.oracle.external.service.sampleservices.GetSampleServiceRs;
import com.oracle.external.service.sampleservices.SampleServiceRq;
import com.oracle.external.service.sampleservices.SampleServiceRs;
import lombok.AllArgsConstructor;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@AllArgsConstructor
public class SampleServiceEndpoint {
    private final RestTemplate restTemplate;
    private static final String NAMESPACE_URI = "http://www.oracle.com/external/services/sampleservices";
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getSampleServiceRq")
    @ResponsePayload
    public GetSampleServiceRs getSampleServiceRq(@RequestPayload SampleServiceRq sampleservicerq) {
        ResponseDTO serviceResponse = restTemplate
                .getForObject("http://localhost:8082/external/services/rest/sample-service", ResponseDTO.class);

        GetSampleServiceRs response = new GetSampleServiceRs();
        SampleServiceRs sampleServiceRs = new SampleServiceRs();
        assert serviceResponse != null;
        sampleServiceRs.setErrorCode(serviceResponse.getSampleservicers().getErrorCode());
        sampleServiceRs.setErrorMsg(serviceResponse.getSampleservicers().getErrorMsg());
        sampleServiceRs.setTrxId(serviceResponse.getSampleservicers().getTrxId().toString());

        response.setSampleservicers(sampleServiceRs);
        return response;
    }
}
