package com.oracle.external.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

@Data
public class ServiceRequestDTO {
    @JsonProperty("service_id")
    private String serviceId;
    @JsonProperty("order_type")
    private String orderType;
    @JsonProperty("type")
    private String type;
    @JsonProperty("trx_id")
    private UUID trxId;
}
