package com.oracle.external.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponseDTO {
    private ServiceResponseDTO sampleservicers;
}
